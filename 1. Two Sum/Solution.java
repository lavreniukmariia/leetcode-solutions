class Solution {
    public int[] twoSum(int[] nums, int target) {
        Map<Integer, Integer> map = new HashMap<>();
        for(int i = 0; i < nums.length; i++) {
            if(map.containsKey(target - nums[i])){
                return new int[] {map.get(target - nums[i]), i};    // if contains searched number, then return result
            }
            map.put(nums[i], i);    // if not, add current number to map and continue
        }
        return new int[] {};
    }
}
