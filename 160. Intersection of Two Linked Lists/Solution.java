/**
 * Definition for singly-linked list.
 * public class ListNode {
 *     int val;
 *     ListNode next;
 *     ListNode(int x) {
 *         val = x;
 *         next = null;
 *     }
 * }
 */
public class Solution {
    public ListNode getIntersectionNode(ListNode headA, ListNode headB) {
      /*Set<ListNode> nodes = new HashSet<>();

        ListNode nA = headA;
        while(nA != null) {
            nodes.add(nA);
            nA = nA.next;
        }

        ListNode nB = headB;
        while(nB != null) {
            if(nodes.contains(nB)){
                return nB;
            }
            nodes.add(nB);
            nB = nB.next;
        }

        return null;*/

        ListNode a = headA;
        ListNode b = headB;

        while (a != b) {
            a = (a == null) ? headB : a.next;
            b = (b == null) ? headA : b.next;
        }

        return a;
        
    }
}
