class Solution {
    public int missingNumber(int[] nums) {
        int n = nums.length;
        int requiredTotal = n*(n+1)/2;  // find total sum that should be
        int realTotal = 0;
        for(int i: nums){
            realTotal += i; // find real sum of array
        }
        return requiredTotal - realTotal;   // substract sums to find the missing number
    }
}
