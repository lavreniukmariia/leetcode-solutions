class Solution {
    public boolean isAnagram(String s, String t) {
        Map<Character, Integer> freq = new HashMap<>();
        for (char c: s.toCharArray()) {
            freq.put(c, freq.getOrDefault(c, 0) + 1);   // Add frequency
        }

        for (char c: t.toCharArray()) {
            freq.put(c, freq.getOrDefault(c, 0) - 1);   // Substract frequency
        }

        for (int i: freq.values()) {
            if (i != 0) {
                return false;   // Сheck if all values are equal to zero
            }
        }

        return true;
    }
}
