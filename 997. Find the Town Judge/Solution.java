class Solution {
    public int findJudge(int n, int[][] trust) {
        int[] trustValues = new int[n+1];
        if(trust.length == 0 && n == 1) {return 1;}
        for(int[] person: trust){
            trustValues[person[0]]--;
            trustValues[person[1]]++;
        }
        for(int i = 0; i < trustValues.length; i++){
            if(trustValues[i] == n-1) {return i;}
        }
        return -1;
    }
}
