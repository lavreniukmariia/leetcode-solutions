/**
 * Definition for a binary tree node.
 * public class TreeNode {
 *     int val;
 *     TreeNode left;
 *     TreeNode right;
 *     TreeNode() {}
 *     TreeNode(int val) { this.val = val; }
 *     TreeNode(int val, TreeNode left, TreeNode right) {
 *         this.val = val;
 *         this.left = left;
 *         this.right = right;
 *     }
 * }
 */
class Solution {
    public List<List<Integer>> levelOrder(TreeNode root) {
        List<List<Integer>> result = new LinkedList<>();
        if(root == null){
            return result;
        }

        Queue<TreeNode> currentLevel = new LinkedList<>();
        currentLevel.add(root);

        while(!currentLevel.isEmpty()){
            List<Integer> currentValues = new LinkedList<>();
            Queue<TreeNode> nextNodes = new LinkedList<>();

            while(!currentLevel.isEmpty()){
                TreeNode node = currentLevel.poll();
                currentValues.add(node.val);

                if(node.left != null) {
                    nextNodes.add(node.left);
                }
                if(node.right != null) {
                    nextNodes.add(node.right);
                }
            }

            result.add(currentValues);
            currentLevel = nextNodes;

        }

        return result;
    }
}
