class LRUCache {
    class Node {
        int key;
        int value;

        Node prev;
        Node next;

        public Node (int key, int value) {
            this.key = key;
            this.value = value;
        }
    }

    int count;
    int capacity;
    Node[] map;
    Node head;
    Node tail;

    public LRUCache(int capacity) {
        this.capacity = capacity;
        count = 0;
        map = new Node[10_000+1];
        head = new Node(0,0);
        tail = new Node(0,0);
        head.next = tail;
        tail.prev = head;
        head.prev = null;
        tail.prev = null;
    }

    public void deleteNode(Node node) {
        node.prev.next = node.next;
        node.next.prev = node.prev;
    }

    public void addNodeToHead(Node node) {
        node.prev = head;
        node.next = head.next;

        head.next = node;
        node.next.prev = node;
    }
    
    public int get(int key) {
        if(map[key] != null) {
            Node node = map[key];
            int getValue = node.value;
            deleteNode(node);
            addNodeToHead(node);
            return getValue;
        }
        return -1;
    }
    
    public void put(int key, int value) {
        if(map[key] != null) {
            Node node = map[key];
            deleteNode(node);
            addNodeToHead(node);
        } else {
            Node node = new Node(key, value);
            map[key] = node;

            if(count < capacity) {
                count++;
            } else {
                map[tail.prev.key] = null;
                deleteNode(tail.prev);
            }
            addNodeToHead(node);
        }
    }
}

/**
 * Your LRUCache object will be instantiated and called as such:
 * LRUCache obj = new LRUCache(capacity);
 * int param_1 = obj.get(key);
 * obj.put(key,value);
 */
